import Vue from 'vue'
import Router from 'vue-router'
import Main from '@/components/Main'
import Habitude from '@/components/habitude/Habitude'
import Footer from '@/components/Footer'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'App',
      components: {
        Main: Main,
        Footer: Footer
      }
    },
    {
      path: '/habitude',
      name: 'Habitude',
      components: {
        Habitude: Habitude
      }
    }
  ]
})
