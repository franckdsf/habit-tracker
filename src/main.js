import Vue from 'vue'
import App from './App.vue'
import router from './router'
import Vuex from 'vuex'
import Store from './store'
import Ionic from '@ionic/vue'

Vue.use(Ionic)
Vue.config.productionTip = false

//  font awesome
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
//  import icons
import { faTimes, faPlus, faAngleDown, faCaretLeft} from '@fortawesome/free-solid-svg-icons'
import { faBell, faCalendar, faListAlt, faNewspaper } from '@fortawesome/free-regular-svg-icons'
library.add(faTimes, faPlus, faAngleDown, faCaretLeft, faCalendar, faBell, faListAlt, faNewspaper)
Vue.component('font-awesome-icon', FontAwesomeIcon)

const store = new Vuex.Store({
  strict: process.env.NODE_ENV !== 'production',
  modules: {
    store: Store
  }
})

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')
